# Avancée et coordination d'équipe

## Mardi 8 février 2022

- Terminé les NixPills, connaissance du langage plus approfondie mais toujours une compréhension du fonctionnement assez basique
- Test sur l'exemple du Readme du depot `nixos-compose`. Des gros soucis lors du build (`nxc build`):

    ```s
    foo: must succeed: true
    foo: waiting for the VM to finish booting
    foo: starting vm
    foo # Formatting '/build/vm-state-foo/foo.qcow2', fmt=qcow2 cluster_size=65536 extended_l2=off compression_type=zlib size=536870912 lazy_refcounts=off refcount_bits=16
    foo # Could not access KVM kernel module: Permission denied
    foo # qemu-system-x86_64: failed to initialize kvm: Permission denied
    foo: QEMU running (pid 6)
    foo: connected to guest root shell
    ```

    Il semblerait que ce soit un problème de permissions, pourtant je suis bien dans le groupe kvm.

- Le build fonctionne avec `-f docker` cependant il ne peux pas start, encore pour une raison de droit (lors d'une création de dossier il indique que le lecteur est en lecture seule).
- A la recherche de solutions à ce souci

## Mercredi 9 février 2022

- Résolution du problème de build; les sites suivants peuvent aider en cas de soucis:
  - En cas de problèmes de droits:
    : <https://www.dedoimedo.com/computers/kvm-permission-denied.html>
  - pour l'installation de KVM:
    : <https://help.ubuntu.com/community/KVM/Installation>
  - En cas de soucis lors du lancement pour lancer le build créé avec docker (`nxc build -f docker`)
    : Si docker est installé avec le gestionnaire de paquet snap, cela peut poser soucis. L'installer selon depuis dépot plutot <https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository>

## Jeudi 10 février 2022

- Switching the daily report to english as requested by the teachers
- Tried to buil my first derivations, using what Antoine used during his previous internship
- Meeting with the team and teachers the morning
- <https://github.com/justinwoo/nix-shorts/tree/master/posts>

## Vendredi 11 février 2022

- Tried to use and undersand k3s
- Titouan helped me since I didn't fully understood
- After this help I now undersand better the packages system

## Monday February 14th 2022

- working as suggested by Titouan: adding nodes (server+agents) on a nig configuration.
- Usage of the flavor `nxc start -I -f nixos-test-driver` to connect to the running build
- Tring to run the build lead me to a better understanding of the global functionning of nixos.
- Nothing more to add, things are still shady in my head by from day to day it gets better. Frequent meeting are welcome since it allow me to clarify some aspects. The one from this morning was useful but I should have prepared more questions. I think I understood where to find the documentation but still, I'm not an expert on k3s nor kubernetes, so I'm still working on it.
- Also I tried to practice a bit on g5k, my account was retired, again.

## Wednesday February 16th 2022

- continuing the work of the previous day. With the following configuration (really close to hat Titouan did) I got the error below.

```nix
{ pkgs, ... }:
let
  testManifest = pkgs.writeText "test.yml" (builtins.readFile ./pod.yaml);
  k3sToken = "df54383b5659b9280aa1e73e60ef78fc";
in
{
  nodes = {
    server = { pkgs, ... }: {
      environment.systemPackages = with pkgs; [ k3s gzip ];

      networking.firewall.allowedTCPPorts = [ 6443 ];
      # k3s uses enough resources the default vm fails.
      virtualisation.memorySize = 1536;
      virtualisation.diskSize = 4096;

      services.k3s.enable = true;
      services.k3s.role = "server";
      services.k3s.package = pkgs.k3s;
      # Slightly reduce resource usage
      # services.k3s.extraFlags = "--no-deploy coredns,servicelb,traefik,local-storage,metrics-server --pause-image test.local/pause:local";
      services.k3s.extraFlags = "--agent-token ${k3sToken}  --disable-network-policy --cluster-cidr 10.24.0.0/16 --flannel-backend=host-gw";
    };
    # --advertise-address xxx --flannel-backend=none --disable traefik

    agent = { pkgs, ...}:{
      environment.systemPackages = with pkgs; [ k3s gzip ];

      # k3s uses enough resources the default vm fails.
      virtualisation.memorySize = 1536;
      virtualisation.diskSize = 4096;

      services.k3s.enable = true;
      services.k3s.package = pkgs.k3s;

      services.k3s.role = "agent";
      services.k3s.serverAddr = "https://10.0.0.10:6443";
      services.k3s.tokenFile = "/var/lib/rancher/k3s/server/node-token";
      services.k3s.token = k3sToken;
      # services.k3s.docker = True;
      # services.k3s.configPath = types.path;
      # Slightly reduce resource usage
      # services.k3s.extraFlags = "--no-deploy coredns,servicelb,traefik,local-storage,metrics-server --pause-image test.local/pause:local";
      services.k3s.extraFlags = "--node-label --node-name test-node";
    };
  };

  testScript = ''
    start_all()
    server.wait_for_unit("k3s")
    agent.wait_for_unit("k3s")
    agent.succeed("kubectl cluster-info dump")
    server.succeed("k3s kubectl cluster-info")
    agent.succeed("k3s kubectl cluster-info")
    server.fail("sudo -u noprivs k3s kubectl cluster-info")
    server.succeed("[ $(k3s kubectl get node --no-headers=true | wc -l) -ne 4 ] && exit 1 || exit 0")

    server.shutdown()
  '';
}
```

The error:

```s
server # [   58.530392] k3s[653]: E0216 16:11:35.375152     653 kubelet.go:2211] "Container runtime network not ready" networkReady="NetworkReady=false reason:NetworkPluginNotReady message:Network plugin returns error: cni plugin not initialized"
agent # [   59.382338] k3s[632]: time="2022-02-16T16:11:36.250115893Z" level=error msg="failed to get CA certs: Get \"https://127.0.0.1:6444/cacerts\": context deadline exceeded (Client.Timeout exceeded while awaiting headers)"
```

after using the flags for the server `--flannel-backend=host-gw` we now get the error in loop (like hundred of times per sec):

```s
server # [  179.170448] k3s[659]: Trace[566009550]: ---"About to apply patch" 36ms (16:59:00.814)
```

## Thursday February 17th 2022

- invastigating on the previous error

for the server
`failed to get sandbox image \"rancher/pause:3.1\": failed to pull image \"rancher/pause:3.1\"`

`agent # [   44.575437] k3s[683]: time="2022-02-17T17:23:07.430981243Z" level=error msg="failed to get CA certs: Get \"https://127.0.0.1:6444/cacerts\": read tcp 127.0.0.1:45410->127.0.0.1:6444: read`

## Monday February 21th 2022

- deploying k3d on nix on g5k to check if it works. It doesn't.
- Meeting with Corentin to disscuss about possible issues, misconfiguration or solutions without any solution.
- Still the same problem of certificates and ip adresses
- The library closed today at 5pm

## Tuesday February 22th

- by looking at Titouan's work, I havent alowed the port `6443` on the client and the server adress is hard coded on the client side, but `"https://server:6443"` is in fact also accepted. This solved the problem of certificates and most connexions problems in the cluster. Jonathan said Titouan's version is working on g5k. That's why I took a look at it, his version is almost the same as mine
- Jonathan tip: use `networking.firewall.enable =false`
- K3s is now functionning as expected

## Wednesday February 23rd

- trying to create the derivation for hadoop using the following files:
  - `nixpkgs/nixos/modules/services/cluster/hadoop/` which contains the files of the hadoop suppport on Nixos. This allows me to know all the options available as well as some examples
  - In `nixpkgs/pkgs/applications/networking/cluster/hadoop` we can find a derivation of hadoop that can be the starting point of mine

## Thursday February 24th

- meeting with Jonathan, he explained me what is inside the different following folders
  - `./nixos/modules/services/cluster/hadoop:` containing all the options that can be set on a package
  - `./nixos/tests/hadoop:` containing an example of a configuration. In this case it tests too many things and for our tests it needs to be changed a bit
  - `./pkgs/applications/networking/cluster/hadoop:` containing the installation procedure of the package: The derivation
- I finally understand how everything works together and where to find information

## Friday February 25th

- I had an error yesterday about the option `services.hadoop.hdfs.namenode.enable` we took a part of the afternoon to fix it with J and Q + someone else. We discovered that the problem was about the flake.lock which is way to old and using old dependencies. The option I tried to use wasn't existing before and was named `services.hadoop.hdfs.namenode.enable` one year ago. The problem was that when initialazing the nix project I used `nxc init -e basic` which copies the files including the flakes.lock from somewhere and was a really old version.
- Later this afternoon I continued to work on hadoop. I created the config using what I found online to create a basic one. I documented this in the file.
- I also followed the tutorial on the hadoop page: <https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/SingleCluster.html>
- I got the following error but I don't know if it is a big deal `namenode # [   16.355049] hdfs-namenode[929]: 2022-02-25 17:20:09,278 WARN impl.MetricsConfig: Cannot locate configuration: tried hadoop-metrics2-namenode.properties,hadoop-metrics2.properties`
- also, when everything seems to be done when starting nix, it gets 'stuck' on this line `namenode # [   21.048316] hdfs-namenode[929]: 2022-02-25 17:20:13,971 INFO blockmanagement.CacheReplicationMonitor: Starting CacheReplicationMonitor with interval 30000 milliseconds` and nothing else happend. It's like the testscript is done but it's not.
- Next time :
  - run a job on yarn on the hadoop page
  - What is httpfs?
  - need more node? which ones and what kind of services

## Tuesday March 1st

- adding the datanode. Connexion problems. Looks like its the port that was specified for the hdfs node (9000), we don't need to specify one and hadoop use one by default.
- I don't remember how to access nodes on nix with the interactive mode. We can use `nodename.command` to do so.
- The datanode is working as expected and can add a file to the hdfs via httpdfs. Httpdfs is the way to go to use the hdfs of hadoop
- Tried to build with the docker flavor and got the error:

```s
       > Run-time dependency xkbcommon found: YES 1.3.1
       > Run-time dependency iso-codes found: YES 4.9.0
       > Run-time dependency tracker-sparql-3.0 found: YES 3.2.1
       > Did not find CMake 'cmake'
       > Found CMake: NO
       > Run-time dependency cairo-xlib found: NO (tried pkgconfig)
       >
       > meson.build:521:97: ERROR: Dependency "cairo-xlib" not found, tried pkgconfig
       >
       > A full log can be found at /build/gtk+-3.24.31/build/meson-logs/meson-log.txt
       For full logs, run 'nix log /nix/store/a1lqg4xvkn9kb5fqcv5a7dcvdkbabdpm-gtk+3-3.24.31.drv'.
```

It appears that the solution is to set `config.environment.noXlibs = pkgs.lib.mkForce false;` in each node we need. `config.environment.noXlibs = false;` works better for me.
<https://github.com/NixOS/nixpkgs/issues/119841>
<https://github.com/NixOS/nixpkgs/issues/102137>

## Friday March 4th

- start writing the workflow of hadoop
- fixed the bug with the previous solutions. See the composition or the workflow file.
- Starting adding more nodes
  - Yarn packet manager
  - 2 ressources nodes (it was in the examples, should explain why)
  - 1 node manager
- I first had to understand how this works.

problem with the connexion of the node manager to the ressource manager, got the error : `nm1 # [  278.224320] yarn-nodemanager[672]: 2022-03-05 20:19:33,048 INFO ipc.Client: Retrying connect to server: 0.0.0.0/0.0.0.0:8031. Already tried 9 time(s); retry policy is RetryUpToMaximumCountWithF)`
The problem was because I had 2 ressources manager nodes and I should write `"yarn.resourcemanager.hostname" = "rm1";` instead of `"yarn.resourcemanager.hostname.rm1" = "rm1";` as it is written in the examples. I still don't know why but it is probably for the high availability.

- `HA` stands for high availability. The examples
- Tested the node cluster with a nodemanager and a ressourcemanager, It works with the tests. But there is something with the hdfs "high availability" that I need to check later.
- Zookeeper can only be used with high availability hdfs. Why does a coordination service needs high availability ?
- I also tried the docker flavor. I had this error `write: No space left on device` for apparently the namenode. Adding the option `virtualisation.memorySize = 1536; virtualisation.diskSize = 4096;` doesnt work. it says this option doesn't exist.
- TODO: there are functions to check health of hadoop nodes and etc. Try them. Then discuss all the above with the team.

## Monday March 7th

Disscussed with Jonathan about the problem with docker flavor. Using sysctl on the namenode once every node is started we see that the filesystem failed to be mounted. Works on Jonathan's machine. He says that it may be because we have to create partitions with docker and maybe by default we don't have the right to write.

```s
dev-hugepages.mount                    loaded failed failed    Huge Pages File System
sys-fs-fuse-connections.mount          loaded failed failed    FUSE Control File System
nscd.service                           loaded failed failed    Name Service Cache Daemon
```

Tried on g5k with the docker flavor but docker needs to be installed first and it's not possible for me as we need root permissions.

## Wednesday March 9th

- There is a command to have sudo and we can activate docker like this. But there are other problems linked to this and I created an issue there: <https://gitlab.inria.fr/nixos-compose/projet-info5/projets/-/issues/1>

## Thurday

Installing a demo job on hadoop to test and create a user experience.

## Tuesday March 15th

- The demo job is almost working. I used what Antoine did a bit, especially the benchmark with `systemd.services.benchmarking` and `graphite`. But I don't plan to use graphite now as it is way to complex for now. Systemd already whrite periodicaly the perf into a csv.
- Looks like there is a permission problem while runing the demo programm.
